import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { enterView } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  heros: Array<any>=[]; 
  options: BarcodeScannerOptions;
  results:{};
  obj:{};
  constructor(private barcode:BarcodeScanner, public navCtrl: NavController) {
    this.heros=[
      {
        "name":"Cinderella",
        "movie":"Cinderella", 
        "year":"1950",
        "barcode":"036000291452", 
        "price":"9,99"
      },
      {
        "name":"pinnocchio",
        "movie":"pinnocchio",
        "year":"1940",
        "barcode":"1234567890128",
        "price":"12,99"
      }
    ]
  }
  async scanBarcode(){
    this.options ={
      prompt: 'scan a barcode to see the result'
    }
    const results = await this.barcode.scan();
    console.log(this.results);
    document.getElementById("toon").innerHTML = results.text;
     let itemYouWant= null; 
     this.heros.forEach((item)=> {
       if (item.barcode === results.text){
         itemYouWant = item; 
       }
     })

    document.getElementById("ja").innerHTML = itemYouWant.name + "<br>" + itemYouWant.price;
  }
  
}
