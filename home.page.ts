import { Component } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  options:BarcodeScannerOptions; 

  constructor(private barcode:BarcodeScanner, public navCtrl:NavController){
  }
  async scanBarcode(){
    const result=await this.barcode.scan();
    console.log(result);
  }
}

