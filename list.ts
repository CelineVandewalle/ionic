import { AppModule } from './../../app/app.module';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  heros: Array<any>=[]; 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.heros=[
      {
        "name":"Cinderella",
        "movie":"Cinderella", 
        "year":"1950"
      },
      {
        "name":"pinnocchio",
        "movie":"pinnocchio",
        "year":"1940"
      }
 
      
    ]
  }
  shownGroup = null;
  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
 };
 isGroupShown(group) {
    return this.shownGroup === group;
 };
  
  goAnotherPage(){
    this.navCtrl.setRoot(HomePage);
  }
}
